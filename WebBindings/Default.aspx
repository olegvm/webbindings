﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebBindings.Default" %>
<%@ Import Namespace="Binding" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <table border="0px;" cellspacing="3px;">
        <tr>
            <td></td><td>Binding</td><td>Mapping</td><td>DataContext</td><td>Model</td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" Text="First Name:" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="FirstNameBound" Text='<%# sender.Bind("FirstName") %>' />
            </td>
            <td>
                <asp:TextBox runat="server" ID="FirstNameTextBox" Text="<%# Model.FirstName %>" />
            </td>
            <td>
                <asp:Label runat="server" Text="<%# ContextFirstName %>" />
            </td>
            <td>
                <asp:Label runat="server" Text="<%# ModelFirstName %>" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" Text="Last Name:" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="LastNameBound" Text='<%# sender.Bind("LastName") %>' />
            </td>
            <td>
                <asp:TextBox runat="server" ID="LastNameTextBox" Text="<%# Model.LastName %>" />
            </td>
            <td>
                <asp:Label runat="server" Text="<%# _dataContext.LastName %>" />
            </td>
            <td>
                <asp:Label runat="server" Text="<%# Model.LastName %>" />
            </td>
        </tr>
    </table>
    <asp:LinkButton runat="server" ID="PostBackButton" Text="PostBack" />
    </form>
</body>
</html>
