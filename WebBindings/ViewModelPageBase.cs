﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Binding.Interfaces;

namespace WebBindings
{
    public abstract class ViewModelPageBase<Tmodel>: Page, IBindingContainer<Tmodel>
    {
        protected Tmodel _dataContext { get; set; }

        protected Tmodel Model { get; set; }

        protected Action<string> log;

        public ViewModelPageBase()
        {
            _dataContext = InitializeModel();
            Model = InitializeModel();
            Log.Clear();
            log = s => Log.Add(s, Model.ToString(), _dataContext.ToString()
                , ViewState == null || ViewState["VIEW_MODEL"] == null ? string.Empty : ViewState["VIEW_MODEL"].ToString()
                , (FindControl("FirstNameBound") as TextBox == null? string.Empty : string.Format(@"[""{0}"", ""{1}""]", (FindControl("FirstNameBound") as TextBox).Text, (FindControl("LastNameBound") as TextBox).Text))
                , (FindControl("FirstNameTextBox") as TextBox == null ? string.Empty : string.Format(@"[""{0}"", ""{1}""]", (FindControl("FirstNameTextBox") as TextBox).Text, (FindControl("LastNameTextBox") as TextBox ?? new TextBox()).Text))
            );
            log("ctor.");
        }

        protected abstract Tmodel InitializeModel();

        protected abstract void WireEvents();

        #region Page lifecycle events

        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);
            log("OnPreInit");
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            log("OnInit");
        }

        protected override void OnInitComplete(EventArgs e)
        {
            base.OnInitComplete(e);
            log("OnInitComplete");
        }

        protected override void OnPreLoad(EventArgs e)
        {
            base.OnPreLoad(e);
            log("OnPreLoad");
        }

        protected static int _count = 0;
        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);
            log("OnDataBinding" + (_count++).ToString());
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            log("OnLoad");
        }

        public override void Validate()
        {
            base.Validate();
            log("Validate");
        }

        public override void Validate(string validationGroup)
        {
            base.Validate(validationGroup);
            log("Validate(string validationGroup)");
        }

        //
        // Controls postback events go here...
        //

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            log("OnLoadComplete");
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            log("OnPreRender");
        }

        protected override void OnPreRenderComplete(EventArgs e)
        {
            base.OnPreRenderComplete(e);
            log("OnPreRenderComplete");
        }

        protected override void OnSaveStateComplete(EventArgs e)
        {
            base.OnSaveStateComplete(e);
            log("OnSaveStateComplete");
        }

        protected override void Render(HtmlTextWriter writer)
        {
            log("Render");
            base.Render(writer);
            writer.WriteFullBeginTag("table");
            writer.WriteFullBeginTag("tr");
            writer.WriteFullBeginTag("th");
            writer.WriteEncodedText("#");
            writer.WriteEndTag("th");
            writer.WriteFullBeginTag("th");
            writer.WriteEncodedText("Event:");
            writer.WriteEndTag("th");
            writer.WriteFullBeginTag("th");
            writer.WriteEncodedText("Model:");
            writer.WriteEndTag("th");
            writer.WriteFullBeginTag("th");
            writer.WriteEncodedText("Context:");
            writer.WriteEndTag("th");
            writer.WriteFullBeginTag("th");
            writer.WriteEncodedText("ViewState:");
            writer.WriteEndTag("th");
            writer.WriteFullBeginTag("th");
            writer.WriteEncodedText("ControlsB:");
            writer.WriteEndTag("th");
            writer.WriteFullBeginTag("th");
            writer.WriteEncodedText("ControlsM:");
            writer.WriteEndTag("th");
            writer.WriteEndTag("tr");
            foreach (var entry in Log.Entries)
            {
                writer.WriteFullBeginTag("tr");
                writer.WriteFullBeginTag("td");
                writer.WriteEncodedText(entry.Value.Item1.ToString());
                writer.WriteEndTag("td");
                writer.WriteFullBeginTag("td");
                writer.WriteEncodedText(entry.Key);
                writer.WriteEndTag("td");
                writer.WriteFullBeginTag("td");
                writer.WriteEncodedText(entry.Value.Item2);
                writer.WriteEndTag("td");
                writer.WriteFullBeginTag("td");
                writer.WriteEncodedText(entry.Value.Item3);
                writer.WriteEndTag("td");
                writer.WriteFullBeginTag("td");
                writer.WriteEncodedText(entry.Value.Item4);
                writer.WriteEndTag("td");
                writer.WriteFullBeginTag("td");
                writer.WriteEncodedText(entry.Value.Item5);
                writer.WriteEndTag("td");
                writer.WriteFullBeginTag("td");
                writer.WriteEncodedText(entry.Value.Item6);
                writer.WriteEndTag("td");
                writer.WriteEndTag("tr");
            }
            writer.WriteEndTag("table");
        }

        protected override void OnUnload(EventArgs e)
        {
            _count = 0;
            base.OnUnload(e);
        }

        #endregion

        #region IBindingContainer<Tmodel> Members

        public Tmodel TypedDataContext
        {
            get { return _dataContext; }
        }

        #endregion

        #region IBindingContainer Members

        public object DataContext
        {
            get
            {
                return _dataContext;
            }
            set
            {
                _dataContext = (Tmodel)value;
            }
        }

        #endregion
    }
}