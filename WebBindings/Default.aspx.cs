﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Binding;
using Binding.Interfaces;
using FuncMapper;

namespace WebBindings
{
    public partial class Default : ViewModelPageBase<UserInfo>, IUserInfoView
    {
        protected override void  WireEvents()
        {
            FirstNameTextBox.TextChanged += (s, e) => log("FirstNameTextBox.TextChanged");
            PostBackButton.Click += (s, e) =>
            {
                TypedDataContext.FirstName += string.Format("_{0}", _count++);
                Model.FirstName += string.Format("_{0}", _count++);
                log("PostBackButton.Click (Model changed)");
            };
        }

        #region Page lifecycle events

        protected void Page_Init(object sender, EventArgs e)
        {
            WireEvents();
        }

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            //after view state is available but before the onload event which is listened to by the binder
            this.RegisterForBinding(StateMode.Recreate);
            if (IsPostBack) // <- just to keep initial state of Model
            {
                Model.InjectFromProperties(this as IUserInfoView);
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            this.DataBind();
        }

        #endregion

        protected override UserInfo InitializeModel()
        {
            return new UserInfo { FirstName = "Oleg", LastName = "Musienko" };
        }

        protected string ModelFirstName
        {
            get
            {
                return Model.FirstName;
            }
            set
            {
                Model.FirstName = value;
            }
        }

        protected string ContextFirstName
        {
            get
            {
                return TypedDataContext.FirstName;
            }
            set
            {
                TypedDataContext.FirstName = value;
            }
        }

        #region IUserInfoView Members

        public string FirstName
        {
            get
            {
                return FirstNameTextBox.Text;
            }
            set
            {
                FirstNameTextBox.Text = value;
            }
        }

        public string LastName
        {
            get
            {
                return LastNameTextBox.Text;
            }
            set
            {
                LastNameTextBox.Text = value;
            }
        }

        #endregion
    }

    internal interface IUserInfoView
    {
        string FirstName { get; set; }
        string LastName { get; set; }
    }

    [Serializable]
    public class UserInfo
    {
        private string _firstName;
        public string FirstName
        {
            get
            {
                return _firstName;
            }
            set
            {
                _firstName = value;
            }
        }

        private string _lastName;
        public string LastName
        {
            get
            {
                return _lastName;
            }
            set
            {
                _lastName = value;
            }
        }

        public override string ToString()
        {
            return string.Format(@"[""{0}"", ""{1}""]", FirstName, LastName);
        }
    }

    public static class Log
    {
        private static readonly Dictionary<string, Tuple<int, string, string, string, string, string>> _log =
            new Dictionary<string, Tuple<int, string, string, string, string, string>>();

        private static int _index = 0;

        public static void Clear()
        {
            _log.Clear();
            _index = 0;
        }

        public static void Add(string key, string model, string context, string viewstate, string controlsB, string controlsM)
        { 
            _log.Add(key, Tuple.Create(_index++, model, context, viewstate, controlsB, controlsM));
        }

        public static IEnumerable<KeyValuePair<string, Tuple<int, string, string, string, string, string>>> Entries
        {
            get { return _log; }
        }
    }
}