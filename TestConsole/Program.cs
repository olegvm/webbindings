﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FuncMapper;

namespace TestConsole
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var foo1 = new Foo();

            var foo2 = new Foo();

            var foo3 = new Foo();

            TypeActionsCache_old<Foo>.SetValue(foo1, "ID", 1);
            TypeActionsCache_old<Foo>.SetValue(foo2, "ID", 2);
            TypeActionsCache_old<Foo>.SetValue(foo3, "ID", 3);

            TypeActionsCache_old<Foo>.SetValue(foo1, "Name", "1");
            TypeActionsCache_old<Foo>.SetValue(foo2, "Name", "2");
            TypeActionsCache_old<Foo>.SetValue(foo3, "Name", "3");

            var conn = new System.Data.SqlClient.SqlConnection("data source=.;initial catalog=basket;");
            TypeActionsCache_old<System.Data.SqlClient.SqlConnection>.SetValue(conn, "ConnectionString", "data source=.;initial catalog=contacts;");

            Console.WriteLine(foo1.ID);
            Console.WriteLine(foo2.ID);
            Console.WriteLine(foo3.ID);

            Console.WriteLine(foo1.Name);
            Console.WriteLine(foo2.Name);
            Console.WriteLine(foo3.Name);

            Console.WriteLine(conn.ConnectionString);

            Console.WriteLine(TypeActionsCache_old<Foo>.GetSetterProviderCallsCount);
            Console.WriteLine(TypeActionsCache_old<Foo>.SetValueCallsCount);

            Console.WriteLine(TypeActionsCache_old<System.Data.SqlClient.SqlConnection>.GetSetterProviderCallsCount);
            Console.WriteLine(TypeActionsCache_old<System.Data.SqlClient.SqlConnection>.SetValueCallsCount);

            Console.ReadLine();
        }
    }

    public class Foo
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
