﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using FuncMapper.Core;
using FuncMapper.Injection;

namespace FuncMapper.Tests.Injection
{
    /// <summary>
    /// Summary description for TypeActionsCacheTests
    /// </summary>
    [TestClass]
    public class TypeActionsCacheTests
    {
        [TestMethod]
        public void _00_create_default_instance()
        {
            var actual = new Foo();

            Assert.IsNotNull(actual);
            Assert.AreEqual(default(Guid), actual.ID);
            Assert.AreEqual(default(string), actual.Name);
            Assert.AreEqual(default(DateTime), actual.StartDate);
            Assert.AreEqual(default(DateTime), actual.EndDate);
            Assert.AreEqual(default(int), actual.StatusID);
            Assert.AreEqual(default(decimal), actual.Price);
            Assert.AreEqual(default(Status), actual.Status);
        }

        [TestMethod]
        public void _01_apply_setters()
        {
            var id = Guid.NewGuid();
            var actual = new Foo();

            actual.SetValue("ID", id);
            actual.SetValue("Name", "Foo");
            actual.SetValue("StartDate", default(DateTime).AddDays(1));
            actual.SetValue("EndDate", default(DateTime).AddDays(2));
            actual.SetValue("StatusID", 555);
            actual.SetValue("Price", (decimal)444.333);
            actual.SetValue("Status", Status.Testing);

            Assert.IsNotNull(actual);
            Assert.AreEqual(id, actual.ID);
            Assert.AreEqual("Foo", actual.Name);
            Assert.AreEqual(default(DateTime).AddDays(1), actual.StartDate);
            Assert.AreEqual(default(DateTime).AddDays(2), actual.EndDate);
            Assert.AreEqual(555, actual.StatusID);
            Assert.AreEqual((decimal)444.333, actual.Price);
            Assert.AreEqual(Status.Testing, actual.Status);
        }

        [TestMethod]
        public void _02_apply_getters()
        {
#if DEBUG
        _isAloneOverheadTest = false;
#endif
            var id = Guid.NewGuid();
            var actual = new Foo
            {
                ID = id,
                Name = "Foo",
                StartDate = default(DateTime).AddDays(1),
                EndDate = default(DateTime).AddDays(2),
                StatusID = 555,
                Price = (decimal)444.333,
                Status = Status.Testing
            };

            var _id = actual.GetValue<Foo, Guid>("ID");
            var _nm = actual.GetValue<Foo, string>("Name");
            var _sd = actual.GetValue<Foo, DateTime>("StartDate");
            var _ed = actual.GetValue<Foo, DateTime>("EndDate");
            var _si = actual.GetValue<Foo, int>("StatusID");
            var _pr = actual.GetValue<Foo, decimal>("Price");
            var _st = actual.GetValue<Foo, Status>("Status");

            Assert.IsNotNull(actual);
            Assert.AreEqual(actual.ID, _id);
            Assert.AreEqual(actual.Name, _nm);
            Assert.AreEqual(actual.StartDate, _sd);
            Assert.AreEqual(actual.EndDate, _ed);
            Assert.AreEqual(actual.StatusID, _si);
            Assert.AreEqual(actual.Price, _pr);
            Assert.AreEqual(actual.Status, _st);
        }

        [TestMethod]
        public void _03_mulptiply_appling_getter_or_setter_does_not_produce_overhead()
        {
#if DEBUG
            SingletonProviderCacheCounter<Foo>.CreateCacheItemCallsCount = 0;
#endif
            var foo = new Foo();

            var actual = Enumerable.Range(1, 9).Select(i =>
            {
                foo.SetValue("StatusID", i);
                return foo.GetValue<Foo,int>("StatusID") == i;
            }).All(b => b);

            Assert.IsTrue(actual);

            actual = Enumerable.Range(1, 9).Select(i =>
            {
                foo.SetValue("Price", (decimal)i);
                return foo.GetValue<Foo, decimal>("Price") == (decimal)i;
            }).All(b => b);

            actual = Enumerable.Range(0, 9).Select(i =>
            {
                foo.SetValue("Status", (Status)i);
                return foo.GetValue<Foo, Status>("Status") == (Status)i;
            }).All(b => b);

            Assert.IsTrue(actual);
#if DEBUG
            Assert.AreEqual(2, SingletonProviderCacheCounter<Base64FormattingOptions>.CreatePayloadCallsCount); 
                        // ^^^ 2 here because of getters & setters have been created by the same base SingletonCache class.
            Assert.AreEqual(_isAloneOverheadTest ? 3 : 0, SingletonProviderCacheCounter<Foo>.CreateCacheItemCallsCount);
                                            // ^^^ 3 here because of each getter used in the current test, 
                                            //     (StatusID, Price, Status).
                                            //     0 here because of setters & getters have been cached by previous tests,
                                            //     and the value has cleaned up in the beginning of the current test.
#endif
        }

#if DEBUG
        private static bool _isAloneOverheadTest = true;
#endif

        [TestMethod]
        public void _04_apply_setters_from_different_values_Guid()
        {
            var actual = new Foo();
            var expected = Guid.NewGuid();

            actual.SetValue("ID", expected);
            Assert.AreEqual(expected, actual.ID);

            actual.SetAdjustedValue<Foo, Guid>("ID", (object)expected);
            Assert.AreEqual(expected, actual.ID);

            actual.SetAdjustedValue<Foo, Guid>("ID", expected.ToString());
            Assert.AreEqual(expected, actual.ID);

            actual.SetAdjustedValue<Foo, Guid>("ID", null);
            Assert.AreEqual(default(Guid), actual.ID);

            actual.SetAdjustedValue<Foo, Guid>("ID", string.Empty);
            Assert.AreEqual(default(Guid), actual.ID);
        }

        [TestMethod]
        public void _05_apply_setters_from_different_values_String()
        {
            var actual = new Foo();
            var expected = "FooName";

            actual.SetValue("Name", expected);
            Assert.AreEqual(expected, actual.Name);

            actual.SetAdjustedValue<Foo, string>("Name", (object)expected);
            Assert.AreEqual(expected, actual.Name);

            actual.SetAdjustedValue<Foo, string>("Name", null);
            Assert.AreEqual(default(string), actual.Name);

            actual.SetAdjustedValue<Foo, string>("Name", string.Empty);
            Assert.AreEqual(string.Empty, actual.Name);
        }

        [TestMethod]
        public void _06_apply_setters_from_different_values_DateTime()
        {
            var actual = new Foo();
            var expected = DateTime.Today;

            actual.SetValue("StartDate", expected);
            Assert.AreEqual(expected, actual.StartDate);

            actual.SetAdjustedValue<Foo, DateTime>("StartDate", (object)expected);
            Assert.AreEqual(expected, actual.StartDate);

            actual.SetAdjustedValue<Foo, DateTime>("StartDate", null);
            Assert.AreEqual(default(DateTime), actual.StartDate);

            actual.SetAdjustedValue<Foo, DateTime>("StartDate", string.Empty);
            Assert.AreEqual(default(DateTime), actual.StartDate);

            actual.SetAdjustedValue<Foo, DateTime>("StartDate", expected.ToString("s"));
            Assert.AreEqual(expected, actual.StartDate);
        }

        [TestMethod]
        public void _07_apply_setters_from_different_values_Int32()
        {
            var actual = new Foo();
            var expected = 42;

            actual.SetValue("StatusID", expected);
            Assert.AreEqual(expected, actual.StatusID);

            actual.SetAdjustedValue<Foo, int>("StatusID", (object)expected);
            Assert.AreEqual(expected, actual.StatusID);

            actual.SetAdjustedValue<Foo, int>("StatusID", null);
            Assert.AreEqual(default(int), actual.StatusID);

            actual.SetAdjustedValue<Foo, int>("StatusID", string.Empty);
            Assert.AreEqual(default(int), actual.StatusID);

            actual.SetAdjustedValue<Foo, int>("StatusID", expected.ToString(CultureInfo.InvariantCulture));
            Assert.AreEqual(expected, actual.StatusID);

            actual.SetAdjustedValue<Foo, int>("StatusID", (long)expected);
            Assert.AreEqual(expected, actual.StatusID);

            actual.SetAdjustedValue<Foo, int>("StatusID", (short)expected);
            Assert.AreEqual(expected, actual.StatusID);

            actual.SetAdjustedValue<Foo, int>("StatusID", (byte)expected);
            Assert.AreEqual(expected, actual.StatusID);

            actual.SetAdjustedValue<Foo, int>("StatusID", (decimal)expected);
            Assert.AreEqual(expected, actual.StatusID);

            actual.SetAdjustedValue<Foo, int>("StatusID", true);
            Assert.AreEqual(1, actual.StatusID);

            actual.SetAdjustedValue<Foo, int>("StatusID", false);
            Assert.AreEqual(0, actual.StatusID);
        }

        [TestMethod]
        public void _08_apply_setters_from_different_values_Decimal()
        {
            var actual = new Foo();
            var expected = (decimal)42.42;

            actual.SetValue("Price", expected);
            Assert.AreEqual(expected, actual.Price);

            actual.SetAdjustedValue<Foo, decimal>("Price", (object)expected);
            Assert.AreEqual(expected, actual.Price);

            actual.SetAdjustedValue<Foo, decimal>("Price", null);
            Assert.AreEqual(default(decimal), actual.Price);

            actual.SetAdjustedValue<Foo, decimal>("Price", string.Empty);
            Assert.AreEqual(default(decimal), actual.Price);

            actual.SetAdjustedValue<Foo, decimal>("Price", expected.ToString(CultureInfo.InvariantCulture));
            Assert.AreEqual(expected, actual.Price);

        }

        [TestMethod]
        public void _09_apply_setters_from_different_values_enum_Status()
        {
            var actual = new Foo();
            var expected = Status.Testing;

            actual.SetValue("Status", expected);
            Assert.AreEqual(expected, actual.Status);

            actual.SetAdjustedValue<Foo, Status>("Status", (object)expected);
            Assert.AreEqual(expected, actual.Status);

            actual.SetAdjustedValue<Foo, Status>("Status", null);
            Assert.AreEqual(Status.Unknown, actual.Status);

            actual.SetAdjustedValue<Foo, Status>("Status", string.Empty);
            Assert.AreEqual(Status.Unknown, actual.Status);

            actual.SetAdjustedValue<Foo, Status>("Status", "Testing");
            Assert.AreEqual(expected, actual.Status);

            actual.SetAdjustedValue<Foo, Status>("Status", 5);
            Assert.AreEqual(expected, actual.Status);
        }
    }

    public class Foo
    { 
        public Guid ID { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int StatusID { get; set; }
        public decimal Price { get; set; }
        public Status Status { get; set; }
    }

    public enum Status
    { 
        Unknown = 0,
        Proposed,
        Investigating,
        Active,
        Pending,
        Testing,
        Resolved,
        Merge,
        Closed
    }
}
