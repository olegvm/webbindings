﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ApprovalTests;
using FuncMapper.Core;

namespace FuncMapper.Tests.Core
{
    /// <summary>
    /// Summary description for GenericVisitorTests
    /// </summary>
    [TestClass]
    public class GenericVisitorTests
    {
        [TestMethod]
        public void _00_create_default_instance()
        {
            var actual = new DefaultTreeWalker<TestTree, StringBuilder>();

            Assert.IsNotNull(actual);
            Assert.IsInstanceOfType(actual, typeof(IContextVisitorAcceptor<TestTree, StringBuilder, IContextVisitor<TestTree, StringBuilder>>));
        }

        [TestMethod]
        public void _01_accept_simple_action()
        {
            var actual = false;
            actual.Visit(b => { actual = true; });

            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void _02_accept_simple_func()
        {
            var actual = false;
            actual = actual.Visit(b => true);

            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void _03_accept_complex_action()
        {
            var actual = string.Empty;
            var foo = new TestTree()
                    .Visit(f => actual = string.Format("{{Name: '{0}', Branch: [{1}], Children: {2}}}"
                        , f.Name, f.Branch, f.Children.Aggregate("", (s, n) => string.Concat(s, "[", n.ToString(), "]"))));

            Assert.AreEqual(foo.ToString(), actual);
        }

        [TestMethod]
        public void _04_accept_simple_visitor_replacement_inside_visit_method()
        {
            var actual = new StringBuilder();
            var foo = TestTree.Default;

            foo.Visit((node) =>
                     {
                         actual.Append("{");
                         actual.AppendFormat("Name: '{0}', ", node.Name);
                         node.Branch.Visit(b => actual.AppendFormat("Branch: [{0}], ", b));
                         actual.Append("Children: ");
                         foreach (var child in node.Children)
                             child.Visit(c => actual.AppendFormat("[{0}]", c));
                         actual.Append("}");
                     });

            Assert.AreEqual(foo.ToString(), actual.ToString());
        }

        [TestMethod]
        public void _05_accept_specified_visitor_combined_with_tree_walking()
        {
            var foo = TestTree.Default;
            var visitor = new TestTreeVisitor();
            visitor.Visit(foo);

            Assert.AreEqual(foo.ToString(), visitor.ToString());
        }

        internal class TestTreeVisitor : DefaultContextVisitor<TestTree, object>
        {
            private StringBuilder sb = new StringBuilder();

            public TestTreeVisitor()
            {
                VisitFunc = (node, context) =>
                {
                    sb.Append("{");
                    sb.AppendFormat("Name: '{0}', Branch: [", node.Name);
                    if (node.Branch != null)
                        Visit(node.Branch);
                    sb.Append("], Children: ");
                    foreach (var child in node.Children)
                    {
                        sb.Append("[");
                        Visit(child);
                        sb.Append("]");
                    }
                    sb.Append("}");

                    return node;
                };
            }

            public override string ToString()
            {
                return sb.ToString();
            }
        }

        [TestMethod]
        public void _06_accept_seperated_visitor_and_tree_walker()
        {
            var foo = TestTree.Default;
            var actual = new TreeStringBuilder();

            actual.Visit(foo);

            //Approvals.Approve(actual.ToString());
            Assert.AreEqual(foo.ToString(), actual.ToString());
        }

        internal class TreeStringBuilder : DefaultContextVisitor<TestTree, StringBuilder>
        {
            private DefaultTreeWalker<TestTree, StringBuilder> walker = new DefaultTreeWalker<TestTree, StringBuilder>();

            public TreeStringBuilder()
            {
                Context = new StringBuilder();
                VisitFunc = (tree, sb) =>
                {
                    sb.Append("{");
                    sb.AppendFormat("Name: '{0}', Branch: [", tree.Name);
                    walker.Accept(tree.Branch, sb, this);
                    sb.Append("], Children: ");
                    walker.AcceptAll(tree.Children, sb, VisitChild);
                    // walker.Accept(tree.Children, sb, this); // <- Alternate way with base method override
                    sb.Append("}");

                    return tree;
                };
            }

            private TestTree VisitChild(TestTree child, StringBuilder sb)
            {
                sb.Append("[");
                var ret = this.Visit(child, sb);
                sb.Append("]");

                return ret;
            }

            // Alternate way to achieve the same results is below:

            //public override IEnumerable<TestTree> VisitAll(IEnumerable<TestTree> children, StringBuilder sb)
            //{
            //    foreach (var child in children)
            //    {
            //        sb.Append("[");
            //        yield return this.Visit(child, sb);
            //        sb.Append("]");
            //    }
            //}

            public override string ToString()
            {
                return Context.ToString();
            }
        }

        [TestMethod]
        public void _07_accept_visitor_with_default_tree_walker()
        {
            var foo = TestTree.Default;
            var actual = new TestTreeWalkerContextVisitor();

            actual.Visit(foo);

            Assert.AreEqual(foo.ToString(), actual.ToString());
        }

        internal class TestTreeWalkerContextVisitor : DefaultContextVisitor<TestTree, StringBuilder>
        {
            public TestTreeWalkerContextVisitor()
            {
                Context = new StringBuilder();
                VisitFunc = (tree, sb) =>
                {
                    sb.Append("{");
                    sb.AppendFormat("Name: '{0}', Branch: [", tree.Name);
                    Visit(tree.Branch, sb); // <- common way to walk through tree (Visit method lives in derrived class context).
                    sb.Append("], Children: ");
                    Acceptor.AcceptAll(tree.Children, sb, VisitChild); // <- specific way to walk through tree.
                    sb.Append("}");

                    return tree;
                };
            }

            private TestTree VisitChild(TestTree child, StringBuilder sb)
            {
                sb.Append("[");
                var ret = this.Visit(child, sb);
                sb.Append("]");

                return ret;
            }

            public override string ToString()
            {
                return Context.ToString();
            }
        }

        [TestMethod]
        public void _08_accept_default_visitor_with_simple_action()
        {
            var expected = TestTree.Default.ToString().Replace("Node", "Default");
            var visitor = new DefaultContextVisitor<TestTree, string>
                          {
                            VisitCoreFunc = (node, name, acceptor) =>
                                {
                                    // Act with node
                                    if (node.Name.StartsWith(name))
                                        node.Name = node.Name.Replace(name, "Default");
                                    // Walk through tree
                                    acceptor.Accept(node.Branch, name);
                                    acceptor.AcceptAll(node.Children, name);
                                    // Return
                                    return node;
                                }
                          };

            var actual = visitor.Visit(TestTree.Default, "Node");

            //Approvals.Approve(expected);
            //Approvals.Approve(actual.ToString());
            Assert.AreEqual(expected, actual.ToString());
        }

        [TestMethod]
        public void _09_accept_default_visitor_with_replace_node_action()
        {
            var expected = TestTree.Default.ToString().Replace("Node", "Replaced");
            int counter = 0;
            var visitor = new DefaultContextVisitor<TestTree, string>
                              {
                                  VisitCoreFunc = (node, name, acceptor) =>
                                  {
                                      TestTree copy;
                                      // Act with node
                                      if (node.Name.StartsWith(name))
                                          copy = new TestTree(string.Format("Replaced{0}", counter++));
                                      else
                                          copy = node;
                                      // Walk through tree with intermediate results
                                      copy.Branch = acceptor.Accept(node.Branch, name);
                                      copy.Children = acceptor.AcceptAll(node.Children, name);
                                      // Return
                                      return copy;
                                  }
                              };

            var actual = visitor.Visit(TestTree.Default, "Node");

            //Approvals.Approve(expected);
            //Approvals.Approve(actual.ToString());
            Assert.AreEqual(expected, actual.ToString());
        }

        [TestMethod]
        public void _10_accept_default_visitor_against_expression_tree()
        {
            var param = Expression.Parameter(typeof(TestTree), "t");
            Expression actual =
                Expression.Lambda<Action<TestTree>>(
                    Expression.Block(
                        Expression.Assign(
                            Expression.MakeMemberAccess(param, typeof(TestTree).GetProperty("Branch")),
                            Expression.Constant(null, typeof(TestTree))
                        ),
                        Expression.Assign(
                            Expression.MakeMemberAccess(param, typeof(TestTree).GetProperty("Name")),
                            Expression.Constant("Foo", typeof(string))
                        )
                    )
                   ,param
                );
            var results = new StringBuilder();

            actual.Visit(results
                , (exp, sb, acceptor) =>
                {
                    // Walk through tree
                    if (exp is LambdaExpression)
                    {
                        acceptor.Accept((exp as LambdaExpression).Body, sb);
                    }
                    else if (exp is BlockExpression)
                    {
                        acceptor.AcceptAll((exp as BlockExpression).Expressions, sb);
                    }
                    else if (exp is BinaryExpression)
                    {
                        acceptor.Accept((exp as BinaryExpression).Left, sb);
                    }
                    else if (exp is MemberExpression)
                    {
                        // Act with node
                        sb.Append((exp as MemberExpression).Member.Name);
                        acceptor.Accept((exp as MemberExpression).Expression, sb);
                    }
                });

            Assert.AreEqual("BranchName", results.ToString());
        }
    }

    internal class TestTree
    {
        private static int _instanceCounter = 0;

        public string Name { get; set; }
        public TestTree Branch { get; set; }
        public IEnumerable<TestTree> Children = new List<TestTree>();

        public TestTree() : this(string.Format("Node{0}", _instanceCounter++), null, new TestTree[] { }) { }

        public TestTree(string name) : this(name, null, new TestTree[] { }) { }

        public TestTree(string name, TestTree node) : this(name, node, new TestTree[] { }) { }

        public TestTree(string name, TestTree node, params TestTree[] nodes)
        {
            Name = name; Branch = node; Children = new List<TestTree>(nodes);
        }

        public override string ToString()
        {
            return string.Format("{{Name: '{0}', Branch: [{1}], Children: {2}}}", Name, Branch
                , Children.Aggregate("", (s, n) => string.Concat(s, "[", n.ToString(), "]"))
                );
        }

        internal static TestTree Default
        {
            get
            {
                _instanceCounter = 0; // <- just to not intersect in approvals from different tests.
                return new TestTree("Root", new TestTree(),
                                new TestTree("Directory01", null,
                                            new TestTree("File01"),
                                            new TestTree("File02", new TestTree()),
                                            new TestTree("File03")
                                        ),
                                new TestTree("Directory02", new TestTree("Subdirectory01"
                                                                    , new TestTree("Link03", new TestTree())
                                                                    , new TestTree("Link04")
                                                                    , new TestTree("Link05", new TestTree())
                                                                 ),
                                            new TestTree("File04", new TestTree()),
                                            new TestTree("File05", new TestTree("Link01", new TestTree())),
                                            new TestTree("File06", new TestTree())
                                        ),
                                new TestTree("Directory03", null,
                                            new TestTree("File07"),
                                            new TestTree("File08", new TestTree("Link02", new TestTree("TestTree", new TestTree()))),
                                            new TestTree("File09")
                                        )
                              );
            }
        }
    }
}
