﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FuncMapper.Tests
{
    /// <summary>
    /// Summary description for MappingTests
    /// </summary>
    [TestClass]
    public class MappingTests
    {
        [TestMethod]
        public void _00_create_default_instance()
        {
            var actual = new FuncMapper.FuncMapper<Foo, Bar>();

            Assert.IsNotNull(actual);
        }

        [TestMethod]
        public void _01_should_implement_IMapper()
        {
            var actual = new FuncMapper.FuncMapper<Foo, Bar>();

            Assert.IsInstanceOfType(actual, typeof(IMapper<Foo, Bar>));
        }

        [TestMethod]
        public void _02_should_map_to_properties_by_names()
        {
            var id = Guid.NewGuid();
            var source = new Foo { ID = id, Name = typeof(Foo).Name, StartDate = DateTime.Today };
            var actual = source.MapTo<Foo, Bar, PropertyNamesInjector<Foo, Bar>>();

            Assert.IsInstanceOfType(actual, typeof(Bar));
            Assert.AreEqual(source.ID, actual.ID);
            Assert.AreEqual(source.Name, actual.Name);
            Assert.AreEqual(source.StartDate, actual.StartDate);
            Assert.AreEqual(actual.EndDate, default(DateTime));
        }

        [TestMethod]
        public void _03_should_map_to_properties_by_dictionary_entries()
        {
            var id = Guid.NewGuid();
            var source = new Dictionary<string, object>
            { 
                { "ID", id },
                { "Name", typeof(Bar).Name},
                { "StartDate", DateTime.Today},
                { "EndDate", DateTime.Today.AddDays(1)},
            };
            var actual = source.MapTo<IDictionary<string, object>, Bar, DictionaryToObjectInjector<Bar>>();

            Assert.IsInstanceOfType(actual, typeof(Bar));
            Assert.AreEqual((Guid)source["ID"], actual.ID);
            Assert.AreEqual((string)source["Name"], actual.Name);
            Assert.AreEqual((DateTime)source["StartDate"], actual.StartDate);
            Assert.AreEqual((DateTime)source["EndDate"], actual.EndDate);
        }

        [TestMethod]
        public void _04_should_map_to_properties_with_different_types()
        {
            var id = Guid.NewGuid();
            var source = new Foo { ID = id, Name = typeof(Foo).Name, StartDate = DateTime.Today, Status = 3 };
            var actual = source.MapTo<Foo, Bar, PropertyNamesInjector<Foo, Bar>>();

            Assert.IsInstanceOfType(actual, typeof(Bar));
            Assert.AreEqual(source.ID, actual.ID);
            Assert.AreEqual(source.Name, actual.Name);
            Assert.AreEqual(source.StartDate, actual.StartDate);
            Assert.AreEqual(default(DateTime), actual.EndDate);
            Assert.AreEqual(Status.Active, actual.Status);
        }
    }

    class FooBase
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
    }

    class Foo : FooBase
    {
        public DateTime StartDate { get; set; }
        public int Status { get; set; }
    }

    class BarBase
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
    }

    class Bar: BarBase
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Status Status { get; set; }
    }

    enum Status
    { 
        Unknown = 0,
        Proposed,
        Investigating,
        Active,
        Pending,
        Testing,
        Resolved,
        Merge,
        Closed
    }
}
