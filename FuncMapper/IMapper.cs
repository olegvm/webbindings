﻿using System;

namespace FuncMapper
{
    public interface IMapper<Tfrom, Tto>
    {
        Exception Exception { get; }
        Tto Map<Tinjector>(Tfrom source) where Tinjector : IValuesInjector<Tfrom, Tto>, new();
        Tto Map<Tinjector>(Tfrom source, bool throwOnMapping) where Tinjector : IValuesInjector<Tfrom, Tto>, new();
        bool TryMap<Tinjector>(Tfrom source, out Tto destination) where Tinjector : IValuesInjector<Tfrom, Tto>, new();
    }
}
