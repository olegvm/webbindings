﻿using System;
using System.Linq.Expressions;

namespace FuncMapper
{
    public interface IValuesInjector<Tfrom, Tto>
    {
        Action<Tfrom, Tto> Injector { get; set; }
        IValuesInjector<Tfrom, Tto> With(Action<Tfrom, Tto> injector);
        IValuesInjector<Tfrom, Tto> With(Expression<Action<Tfrom, Tto>> injectorExpression);
        void InjectValues(Tfrom source, Tto destination);
    }
}
