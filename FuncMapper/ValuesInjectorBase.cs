﻿using System;
using System.Linq.Expressions;

namespace FuncMapper
{
    public abstract class ValuesInjectorBase<Tfrom, Tto> : IValuesInjector<Tfrom, Tto>
    {
        private Action<Tfrom, Tto> _injector = (s, d) => { };

        public Action<Tfrom, Tto> Injector 
        {
            get 
            { 
                return _injector; 
            }
            set
            {
                _injector = value;
            }
        }

        #region IValuesInjector<Tfrom,Tto> Members

        public IValuesInjector<Tfrom, Tto> With(Action<Tfrom, Tto> injector)
        {
            this.Injector = injector;

            return this;
        }

        public IValuesInjector<Tfrom, Tto> With(Expression<Action<Tfrom, Tto>> injectorExpression)
        {
            this.Injector = injectorExpression.Compile();

            return this;
        }

        public void InjectValues(Tfrom source, Tto destination)
        {
            this.Injector(source, destination);
        }

        #endregion
    }
}
