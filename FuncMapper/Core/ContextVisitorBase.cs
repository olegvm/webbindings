﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

using FuncMapper.Injection;

namespace FuncMapper.Core
{
    public class ContextVisitorBase<T, C, A> : IContextVisitor<T, C>
        where A : class, IContextVisitorAcceptor<T, C, IContextVisitor<T, C>>, new()
    {
        protected A Acceptor = new A();

        private Func<T, C, A, T> _visitCoreFunc = (t, c, a) => default(T);
        public Func<T, C, A, T> VisitCoreFunc 
        {
            get { return _visitCoreFunc; }
            set 
            { 
                _visitCoreFunc = value;
                _visitFunc = (t, c) => _visitCoreFunc(t, c, Acceptor);
            } 
        }

        private T VisitCore(T target, C context, A acceptor = null)
        {
            if (target == null)
            {
                return default(T);
            }
            else if (target is A)
            {
                return target.AdjustTypeTo<A>().Accept(target, context, VisitFunc);
            }
            else if (acceptor != null)
            {
                return acceptor.Accept(target, context, VisitFunc);
            }
            else
            {
                return Acceptor.Accept(target, context, VisitFunc);
            }
        }

        public virtual T Visit(T target, A acceptor)
        {
            return this.VisitCore(target, Context, acceptor);
        }

        public virtual T Visit(T target, C context, A acceptor)
        {
            return this.VisitCore(target, context, acceptor);
        }

        public virtual IEnumerable<T> VisitAll(IEnumerable<T> targets, C context, A acceptor)
        {
            foreach (var target in targets)
                yield return this.VisitCore(target, context, acceptor);
        }

        public virtual IEnumerable<T> VisitAll(IEnumerable<T> targets, A acceptor)
        {
            return VisitAll(targets, Context, acceptor);
        }

        #region IContextVisitor<T,C> Members

        public virtual T Visit(T target)
        {
            return this.VisitCore(target, Context);
        }

        public virtual T Visit(T target, C context)
        {
            return this.VisitCore(target, context);
        }

        public virtual IEnumerable<T> VisitAll(IEnumerable<T> targets, C context)
        {
            foreach (var target in targets)
                yield return this.VisitCore(target, context);
        }

        public virtual IEnumerable<T> VisitAll(IEnumerable<T> targets)
        {
            return VisitAll(targets, Context);
        }

        public C Context { get; protected set; }

        private Func<T, C, T> _visitFunc;
        public Func<T, C, T> VisitFunc 
        {
            get { return _visitFunc; }
            set
            {
                _visitFunc = value;
                _visitCoreFunc = (t, c, a) => _visitFunc(t, c);
            }
        }

        #endregion
    }
}
