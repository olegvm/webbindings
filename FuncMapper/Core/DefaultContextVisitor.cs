﻿using System;

namespace FuncMapper.Core
{
    public class DefaultContextVisitor<T, C> : ContextVisitorBase<T, C, DefaultTreeWalker<T, C>> 
    {
        public DefaultContextVisitor()
        {
            Acceptor.Visitor = this;
        }
    }
}
