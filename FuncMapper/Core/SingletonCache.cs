﻿using System;

namespace FuncMapper.Core
{
    public abstract class SingletonCache<Tpayload, Tsingleton> where Tsingleton : SingletonCache<Tpayload, Tsingleton>
    {
        private static readonly Lazy<Tpayload> _lazyPayload = new Lazy<Tpayload>(CreatePayloadCore);
        
        private static Tpayload CreatePayloadCore()
        {
            var @this = TypeActionsCache<Tsingleton>.New();
            var ret = @this.CreatePayload();

            return ret;
        }

        protected abstract Tpayload CreatePayload();

        protected static Tpayload Payload { get { return _lazyPayload.Value; } }
    }
}
