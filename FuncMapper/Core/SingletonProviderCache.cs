﻿using System;
using System.Collections.Concurrent;

namespace FuncMapper.Core
{
    public abstract class SingletonProviderCache<Tcachekey, Tcacheitem, Tsingleton> :
        SingletonCache<Func<Tcachekey, Tcacheitem>, Tsingleton>
        where Tsingleton : SingletonProviderCache<Tcachekey, Tcacheitem, Tsingleton>
    {
        protected override Func<Tcachekey, Tcacheitem> CreatePayload()
        {
#if DEBUG // <- for unit tests
            SingletonProviderCacheCounter<Base64FormattingOptions>.CreatePayloadCallsCount++;
            // ^^^^^^^^^^^ any not usual type to not intersect with all possible test cases
#endif
            var cache = new ConcurrentDictionary<Tcachekey, Tcacheitem>();

            Func<Tcachekey, Tcacheitem> provider = (key) =>
            {
                var ret = cache.GetOrAdd(key, CreateCacheItemCore); // <- dedicated line here for a breakpoint
                return ret;
            };

            return provider;
        }

        // used just to implement Template Method pattern in descendants (since static methods can't be overriden).
        private static Tcacheitem CreateCacheItemCore(Tcachekey key)
        {
            var @this = TypeActionsCache<Tsingleton>.New();
            var ret = @this.CreateCacheItem(key);

            return ret;
        }

        protected abstract Tcacheitem CreateCacheItem(Tcachekey key);
    }
#if DEBUG
    public static class SingletonProviderCacheCounter<T>
    {
        public static int CreatePayloadCallsCount { get; set; }
        public static int CreateCacheItemCallsCount { get; set; }
    }
#endif
}
