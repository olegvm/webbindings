﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuncMapper.Core
{
    public static class ContextVisitorExtensions
    {
        public static T Visit<T>(this T target, Action<T> visit)
        {
            var visitor = new DefaultContextVisitor<T, object> { VisitFunc = (t, c) => { visit(t); return t; } };

            return visitor.Visit(target, null);
        }

        public static T Visit<T>(this T target, Func<T, T> visit)
        {
            var visitor = new DefaultContextVisitor<T, object> { VisitFunc = (t, c) => visit(t) };

            return visitor.Visit(target, null);
        }

        public static T Visit<T, C>(this T target, C context, Func<T, C, T> visit)
        {
            var visitor = new DefaultContextVisitor<T, C> { VisitFunc = visit };

            return visitor.Visit(target, context);
        }

        public static T Visit<T, C>(this T target, C context, Action<T, C> visit)
        {
            var visitor = new DefaultContextVisitor<T, C> { VisitFunc = (t, c) => { visit(t, c); return t; } };

            return visitor.Visit(target, context);
        }

        public static T Visit<T, C>(this T target, C context
                                        , Action<T, C, IContextVisitorAcceptor<T,C,IContextVisitor<T,C>>> visit)
        {
            var visitor = new DefaultContextVisitor<T, C> { VisitCoreFunc = (t, c, a) => { visit(t, c, a); return t; } };

            return visitor.Visit(target, context);
        }

        //public static T Visit<T, V>(this T acceptor, Action<T> action) where V : IVisitor<T>, new()
        //{
        //    var visitor = new V { Visitor = (x) => { action(x); return x; } };

        //    return visitor.Visit(acceptor);
        //}

        //public static T Visit<T>(this T acceptor, Func<T, T> func)
        //{
        //    return acceptor.Visit<T, GenericVisitor<T>>(func);
        //}

        //public static T Visit<T>(this T acceptor, Action<T> action)
        //{
        //    return acceptor.Visit<T, GenericVisitor<T>>(action);
        //}

        //public static T Visit<T>(this T node, Action<T, Action<T>> visit, Action<T> accept)
        //{
        //    var visitor = new GenericVisitor<T> { Visitor = (x) => { visit(x, accept); return x; } };

        //    return visitor.Visit(node);
        //}

        //public static T Visit<T>(this T node, Action<T, Action<T>, Action<T>> visit, Action<T> visit1, Action<T> visit2)
        //{
        //    var visitor = new GenericVisitor<T> { Visitor = (x) => { visit(x, visit1, visit2); return x; } };

        //    return visitor.Visit(node);
        //}

        //public static T Visit<T>(this T node, Action<T, Action<T>, Action<T>, Action<T>> visit, Action<T> visit1, Action<T> visit2, Action<T> visit3)
        //{
        //    var visitor = new GenericVisitor<T> { Visitor = (x) => { visit(x, visit1, visit2, visit3); return x; } };

        //    return visitor.Visit(node);
        //}
    }
}
