﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FuncMapper.Core
{
    public class DefaultTreeWalker<T, C> : IContextVisitorAcceptor<T, C, IContextVisitor<T, C>>
    {
        internal IContextVisitor<T, C> Visitor { get; set; }

        private T AcceptCore(T target, C context, Func<T, C, T> visit)
        {
            if (target == null)
                return default(T);

            return visit(target, context);
        }

        public virtual T Accept(T target)
        {
            return this.AcceptCore(target, Visitor.Context, Visitor.VisitFunc); ;
        }

        public virtual T Accept(T target, C context)
        {
            return this.AcceptCore(target, context, Visitor.VisitFunc); ;
        }

        public virtual IEnumerable<T> AcceptAll(IEnumerable<T> targets, C context)
        {
            return this.AcceptAll(targets, context, Visitor.VisitFunc); ;
        }

        public virtual IEnumerable<T> AcceptAll(IEnumerable<T> targets)
        {
            return this.AcceptAll(targets, Visitor.Context, Visitor.VisitFunc); ;
        }

        public virtual IEnumerable<T> AcceptChildren(T target, C context)
        {
            return AcceptChildren(target, context, Visitor.VisitFunc);
        }

        public virtual IEnumerable<T> AcceptChildren(T target, C context, IContextVisitor<T, C> visitor)
        {
            return AcceptChildren(target, context, visitor.VisitFunc);
        }

        public virtual IEnumerable<T> AcceptChildren(T target, C context, Func<T, C, T> visitItem)
        {
            if (GetChildren == null)
                throw new NotSupportedException("GetChildren can't be null.");

            var targets = GetChildren(target, context);

            return this.AcceptAll(targets, context, visitItem);
        }

        public Func<T, C, IEnumerable<T>> GetChildren { get; set; }

        #region IContextVisitorAcceptor<T, C, IContextVisitor<T, C>> Members

        public virtual T Accept(T target, C context, IContextVisitor<T, C> visitor)
        {
            return this.AcceptCore(target, context, visitor.VisitFunc);
        }

        public virtual T Accept(T target, C context, Func<T, C, T> visit)
        {
            return this.AcceptCore(target, context, visit);
        }

        public virtual IEnumerable<T> AcceptAll(IEnumerable<T> targets, C context, IContextVisitor<T, C> visitor)
        {
            return this.AcceptAll(targets, context, visitor.VisitFunc);
        }

        public virtual IEnumerable<T> AcceptAll(IEnumerable<T> targets, C context, Func<T, C, T> visitItem)
        {
            return targets.Select(item => this.AcceptCore(item, context, visitItem)).ToArray();
        }

        #endregion
    }
}
