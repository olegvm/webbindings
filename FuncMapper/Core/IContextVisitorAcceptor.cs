﻿using System;
using System.Collections.Generic;

namespace FuncMapper.Core
{
    public interface IContextVisitorAcceptor<T, C, V> where V : IContextVisitor<T, C>
    {
        T Accept(T target);
        T Accept(T target, C context);
        T Accept(T target, C context, V visitor);
        T Accept(T target, C context, Func<T, C, T> visit);
        IEnumerable<T> AcceptAll(IEnumerable<T> targets);
        IEnumerable<T> AcceptAll(IEnumerable<T> targets, C context);
        IEnumerable<T> AcceptAll(IEnumerable<T> targets, C context, V visitor);
        IEnumerable<T> AcceptAll(IEnumerable<T> targets, C context, Func<T, C, T> visitItem);
    }
}
