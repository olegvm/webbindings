﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace FuncMapper.Core
{
    public interface IContextVisitor<T, C>
    {
        T Visit(T target, C context);
        T Visit(T target);
        IEnumerable<T> VisitAll(IEnumerable<T> targets, C context);
        IEnumerable<T> VisitAll(IEnumerable<T> targets);
        C Context { get; }
        Func<T, C, T> VisitFunc { get; set; }
    }
}
