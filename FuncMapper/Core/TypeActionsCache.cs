﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using FuncMapper.Core;

namespace FuncMapper.Core
{
    public static class TypeActionsCache<T>
    {
        public static Func<T> New
        {
            get
            {
                return (Func<T>)GetCtor();
            }
        }

        public static Func<T> NewOrDefault
        {
            get
            {
                return (Func<T>)GetCtor() ?? (() => default(T));
            }
        }

        public static Delegate GetCtor(params Type[] argumentTypes)
        {
            var ctor = typeof(T).GetConstructor(argumentTypes);
            if (ctor == null)
                return null;

            var p_args = argumentTypes.Select((t, i) => Expression.Parameter(t, string.Concat("arg", i.ToString())))
                        .ToArray();

            var lambda = Expression.Lambda(
                            Expression.New(ctor, p_args),
                            p_args
                         );

            return lambda.Compile();
        }

        private class SettersCache<Ts> : SingletonProviderCache<string, Delegate, SettersCache<Ts>>
        {
            protected override Delegate CreateCacheItem(string name)
            {
                var pi = typeof(Ts).GetProperty(name);
                var setter = pi.GetSetMethod();
                var instance = Expression.Parameter(typeof(Ts), "instance");
                var value = Expression.Parameter(pi.PropertyType, "value");
                var lambda = Expression.Lambda(
                                Expression.Call(instance, setter, value),
                                instance,
                                value
                             );
                return lambda.Compile();
            }

            internal static void ApplySetter<Tproperty>(Ts instance, string name, Tproperty value)
            {
                Action<Ts, Tproperty> setter = (Action<Ts, Tproperty>)Payload(name);
                setter(instance, value);
            }
        }

        internal static void ApplySetter<P>(T instance, string name, P value)
        {
            SettersCache<T>.ApplySetter(instance, name, value);
        }

        private class GettersCache<Tg> : SingletonProviderCache<string, Delegate, GettersCache<Tg>>
        {
            protected override Delegate CreateCacheItem(string name)
            {
#if DEBUG
                SingletonProviderCacheCounter<T>.CreateCacheItemCallsCount++;
#endif
                var pi = typeof(Tg).GetProperty(name);
                var getter = pi.GetGetMethod();
                var instance = Expression.Parameter(typeof(Tg), "instance");
                var lambda = Expression.Lambda(
                                Expression.Call(instance, getter),
                                instance
                             );
                return lambda.Compile();
            }

            internal static Tproperty ApplyGetter<Tproperty>(Tg instance, string name)
            {
                Func<Tg, Tproperty> getter = (Func<Tg, Tproperty>)Payload(name);
                var ret = getter(instance);

                return ret;
            }

        }

        internal static P ApplyGetter<P>(T instance, string name)
        {
            return GettersCache<T>.ApplyGetter<P>(instance, name);
        }
    }
}
