﻿using System;
using System.ComponentModel;

using FuncMapper.Core;

namespace FuncMapper.Injection
{
    public static class InjectionPrimitives
    {
        public static R AdjustTypeTo<R>(this object value, bool throwIfNotConverted = true)
        {
            R ret = default(R);
            if (value == null)
                return ret;

            var message = string.Format("Can't convert value '{0}' of type '{1}' to type '{2}'.",
                                            value, value.GetType(), typeof(R));
            if (value is R)
                ret = (R)value;
            else if (value is string)
            {
                if ((string)value == string.Empty)
                    return ret;

                if (typeof(R).IsAssignableFrom(typeof(Enum)))
                {
                    ret = (R)Enum.Parse(typeof(R), (string)value);
                }
                else
                {
                    var converter = TypeDescriptor.GetConverter(typeof(R));
                    if (converter.CanConvertFrom(typeof(string)))
                        ret = (R)converter.ConvertFromInvariantString((string)value);
                    else if (throwIfNotConverted)
                        throw new NotSupportedException(message);
                }

            }
            else if (typeof(R).IsEnum && Enum.GetUnderlyingType(typeof(R)) == value.GetType()
                                        && Enum.IsDefined(typeof(R), value))
            {
                ret = (R)value;
            }
            else if (value is IConvertible && typeof(R).IsPrimitive)
            {
                ret = (value as IConvertible).ConvertTo<R>();
            }
            else
            {
                var converter = TypeDescriptor.GetConverter(typeof(R));
                if (converter.CanConvertFrom(value.GetType()))
                    ret = (R)converter.ConvertFrom(value);
                else if (throwIfNotConverted)
                    throw new NotSupportedException(message);
            }

            return ret;
        }

        public static void SetValue<T, P>(this T instance, string name, P value)
        {
            TypeActionsCache<T>.ApplySetter(instance, name, value);
        }

        public static void SetAdjustedValue<T, P>(this T instance, string name, object value)
        {
            var adjusted = value.AdjustTypeTo<P>();
            TypeActionsCache<T>.ApplySetter(instance, name, adjusted);
        }

        public static P GetValue<T,P>(this T instance, string name)
        {
            return TypeActionsCache<T>.ApplyGetter<P>(instance, name);
        }

        public static R ConvertTo<R>(this IConvertible convertible)
        {
            object ret = null;

            switch (Type.GetTypeCode(typeof(R)))
            {
                case TypeCode.Boolean: ret = convertible.ToBoolean(null);
                    break;
                case TypeCode.Byte: ret = convertible.ToByte(null);
                    break;
                case TypeCode.Char: ret = convertible.ToChar(null);
                    break;
                case TypeCode.DBNull: ret = DBNull.Value;
                    break;
                case TypeCode.DateTime: ret = convertible.ToDateTime(null);
                    break;
                case TypeCode.Decimal: ret = convertible.ToDecimal(null);
                    break;
                case TypeCode.Double: ret = convertible.ToDouble(null);
                    break;
                case TypeCode.Empty: 
                    break;
                case TypeCode.Int16: ret = convertible.ToInt16(null);
                    break;
                case TypeCode.Int32: ret = convertible.ToInt32(null);
                    break;
                case TypeCode.Int64: ret = convertible.ToInt64(null);
                    break;
                case TypeCode.Object:
                    break;
                case TypeCode.SByte: ret = convertible.ToSByte(null);
                    break;
                case TypeCode.Single: ret = convertible.ToSingle(null);
                    break;
                case TypeCode.String: ret = convertible.ToString(null);
                    break;
                case TypeCode.UInt16: ret = convertible.ToUInt16(null);
                    break;
                case TypeCode.UInt32: ret = convertible.ToUInt32(null);
                    break;
                case TypeCode.UInt64: ret = convertible.ToUInt64(null);
                    break;
                default:
                    throw new NotSupportedException();
            }

            return (R)ret;
        }
    }
}
