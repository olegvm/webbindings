﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace FuncMapper
{
    public class DictionaryToObjectInjector<Tto> : ValuesInjectorBase<IDictionary<string,object>, Tto>
    {
        public DictionaryToObjectInjector()
        {
            Injector = CreateInjector();
        }

        private Action<IDictionary<string, object>, Tto> CreateInjector()
        {
            var destProps = typeof(Tto).GetProperties(BindingFlags.Instance
                | BindingFlags.Public | BindingFlags.FlattenHierarchy);
            var query = from d in destProps
                        where d.CanWrite
                        select d;

            Expression<Func<IDictionary<string, object>, object>> stub = (d) => d["name"];
            var call_mi = (stub.Body as MethodCallExpression).Method;

            var p_source = Expression.Parameter(typeof(IDictionary<string, object>), "s");
            var p_dest = Expression.Parameter(typeof(Tto), "d");
            var lambda = Expression.Lambda<Action<IDictionary<string, object>, Tto>>(
                            Expression.Block(
                                query.Select(pi => Expression.Assign(
                                                         Expression.MakeMemberAccess(p_dest, pi)
                                                        , Expression.Convert(
                                                             Expression.Call(p_source, call_mi
                                                                , Expression.Constant(pi.Name)
                                                             ) /* Call */
                                                             ,pi.PropertyType
                                                          ) /* Convert */
                                                   ) /* Assign */
                                        ) /* Select */
                            ) /* Block */
                , p_source
                , p_dest);

            return lambda.Compile();
        }
    }
}
