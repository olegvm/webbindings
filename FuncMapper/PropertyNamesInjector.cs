﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace FuncMapper
{
    public class PropertyNamesInjector<Tfrom, Tto>: ValuesInjectorBase<Tfrom, Tto>
    {
        public PropertyNamesInjector()
        {
            Injector = CreateInjector();
        }

        private Action<Tfrom, Tto> CreateInjector()
        {
            var sourceProps = typeof(Tfrom).GetProperties(BindingFlags.Instance
                | BindingFlags.Public | BindingFlags.FlattenHierarchy);
            var destProps = typeof(Tto).GetProperties(BindingFlags.Instance
                | BindingFlags.Public | BindingFlags.FlattenHierarchy);
            var query = from s in sourceProps
                    from d in destProps
                    where s.CanRead && d.CanWrite && s.Name == d.Name
                    select Tuple.Create(s, d);

            var p_source = Expression.Parameter(typeof(Tfrom), "s");
            var p_dest = Expression.Parameter(typeof(Tto), "d");
            var lambda = Expression.Lambda<Action<Tfrom, Tto>>(
                            Expression.Block(
                                query.Select(t => GenerateAssignment(p_source, p_dest, t.Item1, t.Item2))
                            )
                ,p_source
                ,p_dest);

            return lambda.Compile();
        }

        private Expression GenerateAssignment(Expression sourceExpression, Expression destinationExpression
                                                , MemberInfo sourceMemberInfo, MemberInfo destinationMemberInfo)
        {
            if (!IsPropertyOrField(sourceMemberInfo))
                throw new ApplicationException(string.Format(
                        @"Can't inject value from a member '{0}' of type '{1}' since it's not a property or field.",
                        sourceMemberInfo.Name,
                        sourceMemberInfo.DeclaringType));

            if (!IsPropertyOrField(destinationMemberInfo))
                throw new ApplicationException(string.Format(
                        @"Can't inject value into a member '{0}' of type '{1}' since it's not a property or field.",
                        destinationMemberInfo.Name,
                        destinationMemberInfo.DeclaringType));

            var sourceType = GetMemberType(sourceMemberInfo);
            var destinationType = GetMemberType(destinationMemberInfo);

            if (sourceType == destinationType)
            {
                return Expression.Assign(
                            Expression.MakeMemberAccess(sourceExpression, destinationMemberInfo),
                            Expression.MakeMemberAccess(destinationExpression, sourceMemberInfo)
                       );
            }
            else 
            {
                var converter = TypeDescriptor.GetConverter(destinationMemberInfo.ReflectedType);
                if (!converter.CanConvertFrom(sourceMemberInfo.ReflectedType))
                {
                    throw new ApplicationException(string.Format(
                        @"Can't convert from type '{0}' to '{1}'.{2}Convertion the '{3}' member of type '{4}' to the '{5}' member of type '{6}' failed.",
                        sourceType,
                        destinationType,
                        Environment.NewLine,
                        sourceMemberInfo.Name,
                        sourceMemberInfo.DeclaringType.FullName,
                        destinationMemberInfo.Name,
                        destinationMemberInfo.DeclaringType.FullName)
                    );
                }
                    Expression<Func<TypeConverter>> stubGetConverter = () => TypeDescriptor.GetConverter(null);
                    var methodGetConverter = (stubGetConverter.Body as MethodCallExpression).Method;

                    Expression<Func<object>> stubConvertFrom = () => converter.ConvertFrom(null);
                    var methodConvertFrom = (stubConvertFrom.Body as MethodCallExpression).Method;

                    return Expression.Assign(
                            Expression.MakeMemberAccess(sourceExpression, destinationMemberInfo),
                            Expression.Call(
                                Expression.Call(methodGetConverter, Expression.Constant(destinationMemberInfo.ReflectedType)), 
                                methodConvertFrom, 
                                Expression.MakeMemberAccess(destinationExpression, sourceMemberInfo)
                            )
                       );
            }
        }

        private bool IsPropertyOrField(MemberInfo memberInfo)
        {
            return memberInfo.MemberType == MemberTypes.Property || memberInfo.MemberType == MemberTypes.Field;
        }

        private object GetMemberType(MemberInfo memberInfo)
        {
            if (memberInfo.MemberType == MemberTypes.Property)
                return (memberInfo as PropertyInfo).PropertyType;
            else if (memberInfo.MemberType == MemberTypes.Field)
                return (memberInfo as FieldInfo).FieldType;
            else
                throw new NotImplementedException();
        }
    }
}
