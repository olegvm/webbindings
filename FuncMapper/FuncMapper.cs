﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace FuncMapper
{
    public class FuncMapper<Tfrom, Tto> : IMapper<Tfrom, Tto>
    {
        private static readonly Lazy<Func<Tfrom, Tto>> _mapper = new Lazy<Func<Tfrom, Tto>>(BuildMapper);
        private static Action<Tfrom, Tto> _injector = (s, d) => { };

        #region IMapper<Tfrom,Tto> Members

        public Exception Exception { get; private set; }

        public Tto Map<Tinjector>(Tfrom source) where Tinjector : IValuesInjector<Tfrom, Tto>, new()
        {
            return this.Map<Tinjector>(source, true);
        }

        public Tto Map<Tinjector>(Tfrom source, bool throwOnMapping) where Tinjector : IValuesInjector<Tfrom, Tto>, new()
        {
            Tto ret = default(Tto);
            try
            {
                _injector = new Tinjector().Injector;
                ret = _mapper.Value(source);
            }
            catch (Exception ex)
            {
                this.Exception = ex;
                if (throwOnMapping)
                    throw;
            }

            return ret;
        }

        public bool TryMap<Tinjector>(Tfrom source, out Tto destination) 
            where Tinjector : IValuesInjector<Tfrom, Tto>, new()
        {
            destination = this.Map<Tinjector>(source, throwOnMapping: false);

            return this.Exception == null;
        }

        #endregion

        private static Func<Tfrom, Tto> BuildMapper()
        {
            Func<Tfrom, Tto> mapper = source =>
            {
                var ret = NewOrDefault();
                _injector(source, ret);
                return ret;
            };

            return mapper;
        }

        private static Tto NewOrDefault()
        {
            var ret = default(Tto);
            var ctor = typeof(Tto).GetConstructor(new Type[] {});
            if (ctor != null)
            {
                var expNew = Expression.Lambda<Func<Tto>>(
                        Expression.New(ctor)
                    );
                return expNew.Compile().Invoke();
            }

            return ret;
        }
    }
}
