﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace FuncMapper
{
    public static class TypeActionsCache_old<T>
    {
        public static int SetValueCallsCount = 0;
        public static int GetSetterProviderCallsCount = 0;

        private static readonly Lazy<Func<string, Action<T, object>>> _lazySetterProvider =
            new Lazy<Func<string, Action<T, object>>>(GetSetterProvider);
        private static Func<string, Action<T, object>> GetSetterProvider()
        {
            GetSetterProviderCallsCount++;

            var cache = new ConcurrentDictionary<string, Action<T, object>>();

            Func<string, Action<T, object>> ret = (propertyName) =>
            {
                var target = Expression.Parameter(typeof(T), "target");
                var value = Expression.Parameter(typeof(object), "value");
                var pi = typeof(T).GetProperty(propertyName);
                var setter_mi = pi.GetSetMethod();
                
                var setter = Expression.Lambda<Action<T, object>>(
                                Expression.Call(
                                    target, 
                                    setter_mi, 
                                    Expression.Convert(value, pi.PropertyType)
                                ),
                             target, value);
                
                return cache.GetOrAdd(propertyName, setter.Compile());
            };

            return ret;
        }

        public static void SetValue(T target, string propertyName, object value)
        {
            SetValueCallsCount++;
            var setter = _lazySetterProvider.Value(propertyName);
            setter(target, value);
        }
    }
}
