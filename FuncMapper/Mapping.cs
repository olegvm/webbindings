﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuncMapper
{
    public static class Mapping
    {
        public static Tto MapTo<Tfrom, Tto, Tinjector>(this Tfrom source)
            where Tinjector : IValuesInjector<Tfrom, Tto>, new()
        {
            var mapper = new FuncMapper<Tfrom, Tto>();

            return mapper.Map<Tinjector>(source);
        }

        public static void InjectFrom<Tfrom, Tto>(this Tto destination, Tfrom source, IValuesInjector<Tfrom, Tto> injector)
        {
            injector.InjectValues(source, destination);
        }

        public static void InjectFromProperties<Tfrom, Tto>(this Tto destination, Tfrom source)
        {
            var injector = new PropertyNamesInjector<Tfrom, Tto>();
            injector.InjectValues(source, destination);
        }

        public static void InjectFromDictionary<Tfrom, Tto>(this Tto destination, IDictionary<string, object> source)
        {
            var injector = new DictionaryToObjectInjector<Tto>();
            injector.InjectValues(source, destination);
        }
    }
}
